let disciplinas = [];
class Disciplina{
  constructor(disciplina){
    this.disciplina = disciplina;
  }
} 
function Cadastrar(){
  let nomeDisciplina = byId("nomeDisciplina").value;
  let disciplina1 = new Disciplina(nomeDisciplina);
  disciplinas.push(disciplina1);
  apresentarDiscip();
} 
function apresentarDiscip(){
  let tabela = byId("tabela");
  let lista = "<ol>"
  for(let disciplina1 of disciplinas){
    lista += `<li>${disciplina1.disciplina}</li>`
  } 
  lista += "</ol>"
  tabela.innerHTML = lista;
}

function byId(id){
  return document.getElementById(id);
}